package com.androidsample.myfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 *  default  base fragment
 *  call by
 *  new ReplaceFragment(getContext()).replace(((AppCompatActivity)getContext()).getSupportFragmentManager(), R.id.mainFrame, new PageFragment());
 *  new ReplaceFragment(context).addWithBundle(((AppCompatActivity)context).getSupportFragmentManager(), R.id.mainFrame, new PageFragment(), bundle);
 */
public class PageFragment extends Fragment
{
    private int iID = -1 ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_one_recyclerview, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.menu_item_one_button, menu);
        ActionBar actionBar = null;
        if (getActivity() != null)
        {
            actionBar = ((MainActivity) getActivity()).getSupportActionBar();
            if (actionBar != null)
            {
                actionBar.setTitle(""+iID);
            }
        }
        MenuItem complete = menu.findItem(R.id.buttonItem);
        complete.setTitle("Button") ;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        //findViewByIds

        //setUpOnClick

        //getBundles
        if (getArguments() != null)
        {
            iID = getArguments().getInt("Value", -1);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    public static PageFragment newInstance(String sz)
    {
        PageFragment mFragment = new PageFragment();
        Bundle args = new Bundle();
        int iValue = Integer.parseInt(sz) ;
        args.putInt("Value",iValue );
        mFragment.setArguments(args);
        return mFragment;
    }
}