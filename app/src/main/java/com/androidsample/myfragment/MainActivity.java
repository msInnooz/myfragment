package com.androidsample.myfragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    public static final String TAG = "TAGP";
    public static boolean isFragmentEnable = true ;
    //
    private TabLayout mTablayout;
    private ViewPager mViewPager;
    private List<PageView> pageList;

    private int[] imageResourceId = {
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //init Tablayout , ViewPager
        if (isFragmentEnable == false)
        {
            setContentView(R.layout.activity_main);
            initTablayoutAndViewPager() ;
        }
        else
        {
            setContentView(R.layout.activity_fragment);
            initTablayoutAndViewPagerWithFragment() ;
        }
    }

    public void initTablayoutAndViewPager()
    {
        //initData
        pageList = new ArrayList<>();
        pageList.add(new PageOne(MainActivity.this));
        pageList.add(new PageOne(MainActivity.this));
        pageList.add(new PageOne(MainActivity.this));
        pageList.add(new PageOne(MainActivity.this));
        pageList.add(new PageOne(MainActivity.this));
        //initView
        mTablayout = (TabLayout) findViewById(R.id.tabLayout);
        mTablayout.setupWithViewPager(mViewPager);
        mTablayout.addTab(mTablayout.newTab().setText("HTTP").setIcon(R.mipmap.ic_launcher));
        mTablayout.addTab(mTablayout.newTab().setText("FIREBASE").setIcon(R.mipmap.ic_launcher));
        mTablayout.addTab(mTablayout.newTab().setText("VIDEO").setIcon(R.mipmap.ic_launcher));
        mTablayout.addTab(mTablayout.newTab().setText("WEBVIEW").setIcon(R.mipmap.ic_launcher));
        mTablayout.addTab(mTablayout.newTab().setText("NONE").setIcon(R.mipmap.ic_launcher));
        //
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(new SamplePagerAdapter());
        //initListener
        mTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                //Log.d("TagSampleAppProject","onTabSelected");
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {
                Log.d(TAG, "onTabUnselected");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {
                Log.d(TAG, "onTabReselected");
            }
        });
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTablayout));
    }
    //
    public void initTablayoutAndViewPagerWithFragment()
    {
        mTablayout = (TabLayout) findViewById(R.id.tabLayout);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        //setUpViewPager
        MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(5);//?
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                Log.d(TAG, "onPageSelected");
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        mTablayout.setupWithViewPager(mViewPager);
        for (int i = 0; i < MyAdapter.MAX_FRAGMENT; i++)
        {
            mTablayout.getTabAt(i).setIcon(imageResourceId[i]);
        }

        for (int i = 0; i < mTablayout.getTabCount(); i++)
        {
            TabLayout.Tab tab = mTablayout.getTabAt(i);
            if (tab != null)
            {
                //tab.setCustomView(adapter.getTabView(i));
                tab.setCustomView(R.layout.item_tab);
                if (tab.getCustomView() != null)
                {
                    View tabView = (View) tab.getCustomView().getParent();
                    tabView.setTag(i);
                    tabView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            int pos = (int) view.getTag();
                            TabLayout.Tab tab = mTablayout.getTabAt(pos);
                            if (tab != null)
                            {
                                tab.select();
                            }
                        }
                    });
                }
            }
        }
    }
    //inner class
    private class SamplePagerAdapter extends PagerAdapter
    {
        @Override
        public int getCount()
        {
            return pageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o)
        {
            return o == view;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            container.addView(pageList.get(position));
            return pageList.get(position);
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            container.removeView((View) object);
        }
    }

    //inner class
    private class MyAdapter extends FragmentPagerAdapter
    {
        public static final int MAX_FRAGMENT = 5 ;
        MyAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public int getCount()
        {
            return MAX_FRAGMENT ;
        }

        @Override
        public Fragment getItem(int position)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("Value",position) ;
            PageFragment fragment = new PageFragment() ;
            fragment.setArguments(bundle);
            switch (position)
            {
                case 0:
                    return fragment;
                case 1:
                    return fragment;
                case 2:
                    return fragment;
                case 3:
                    return new PageFragment();
                case 4:
                    return fragment;
                default:
                    return null;
            }
        }
    }
}
